stupid vim hackernews client
=============================

This plugin uses [maktaba](https://github.com/google/maktaba) and [glaive](https://github.com/google/glaive) for plugin management.
See Glaive documentation on how to install it.

It is stupid but works.
You can \<leader\>hn to open hacker news, and then o to open an article.

![ScreenShot](https://raw.github.com/stgpetrovic/vim-hackernews/master/screenshot.png)
