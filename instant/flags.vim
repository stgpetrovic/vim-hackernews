" @stylized hackernews
" @tagline hackernews client in VIM
" @order intro usage sources plugins packages config commands functions about
" HackerNews is a ycombinator hacker news client. It depends upon |Maktaba|.

""
" @section Intro, intro
" You can read hacker news directly in VIM or you can open them in a browser.

""
" @section Usage, usage
"
" You must specify the type of rendering you'd like (browser or inline),
" and HackerNews will to the rest. You can define that in your |vimrc|:
" >
"   Glug hackernews style='inline'
" <
"
" You can define a mapping to show Hacker News. To use the default mapping of
" "<Leader>hn", add the following to your vimrc:
" >
"   Glug hackernews plugin[mappings]
" <

let [s:plugin, s:enter] = maktaba#plugin#Enter(expand('<sfile>:p'))
if !s:enter
  finish
endif

""
" Set the style. There are 2 options:
"     1. 'inline' - shows the news in a split buffer
"     2. 'browser' - forks a browser session for the news
call s:plugin.Flag('hackernews_style', 'inline')

""
" Define which browser executable to call, if style is set to browser.
call s:plugin.Flag('hackernews_browser', 'sensible-browser')

""
" Format for the news list. Any valid Python str.format is acceptable,
" Parameters given to format are (points, title, commentCount).
call s:plugin.Flag('hackernews_format', '{:<7} {} ({} comments)')
