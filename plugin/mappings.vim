let [s:plugin, s:enter] = maktaba#plugin#Enter(expand('<sfile>:p'))
if !s:enter
  finish
endif

let s:prefix = s:plugin.MapPrefix('h')
execute 'nnoremap <unique> <script> <silent>' s:prefix . 'n :HackerNews<cr>'
