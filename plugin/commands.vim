let [s:plugin, s:enter] = maktaba#plugin#Enter(expand('<sfile>:p'))
if !s:enter
  finish
endif

""
" Show HackerNews
command -nargs=0 HackerNews call hackernews#Render()
