"{{{ init

if !has("python")
    maktaba#error#Warn('vim has to be compiled with +python to run this')
    finish
endif

let s:script_folder_path = escape(expand('<sfile>:p:h:h'), '\')
let s:plugin = maktaba#plugin#Get('hackernews')

"}}}


"{{{ utility functions

function! s:OpenNews()
  if s:plugin.Flag('hackernews_style') ==# 'browser'
    call maktaba#system#Call(s:plugin.Flag('hackernews_browser') . ' "' . 
          \ s:GetUrl() . '"')
  else
    let l:ln = line(".") - 4
    exec 'silent pedit [Article]'
    wincmd P | wincmd H
    setlocal buftype=nofile
    setlocal noswapfile
    set modifiable

    execute 'python WriteArticle(' . l:ln . ')'
  endif
endfunction

python << EOF
import unicodedata
import urllib2
import vim
import subprocess
import json
import sys
import os

HN_URL = 'http://api.ihackernews.com/page'
URL = {}

def WriteHackerNews():
  news = []
  fmt = vim.eval('s:plugin.Flag("hackernews_format")')
  buff = int(vim.eval('s:plugin.globals.buffer')) - 1
  data = json.loads(urllib2.urlopen(HN_URL).read()).items()[0][1]
  for i, item in enumerate(data):
    ttle = unicodedata.normalize('NFKD', item['title']).encode('ascii','ignore')
    URL[i] = unicodedata.normalize('NFKD', item['url']).encode('ascii','ignore')
    news.append(fmt.format('[%s]' % item['points'], ttle, item['commentCount']))

  vim.buffers[buff].append('Hacker New for vim', 0)
  vim.buffers[buff].append('------------------', 1)
  vim.buffers[buff].append(news, 3)

def WriteArticle(line):
  url = URL[line]
  binary = os.path.join(vim.eval('s:script_folder_path'), 'python/html2text.py')
  args = [sys.executable, binary, url]
  vim.current.buffer.append(subprocess.check_output(args).split('\n'), 0)
EOF

function! s:GetUrl()
python << EOF
vim.command('return "%s"' % URL[int(vim.eval('line(".")')) - 4])
EOF
endfunction


function! s:GetHackerNews()
  exec 'silent pedit [HackerNews]'

  wincmd O
  let s:plugin.globals.buffer = bufnr('%')
  setlocal buftype=nofile
  setlocal noswapfile
  setlocal ft=hackernews
  set modifiable

  command! -buffer -nargs=0 OpenNews call s:OpenNews()
  nnoremap <buffer> o :OpenNews<cr>

  python WriteHackerNews()
endfunction
"}}}


"{{{ public interface

function! hackernews#Render()
  call s:GetHackerNews()
endfunction

"}}}
